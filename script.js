function filterBy(array, type) {
    return array.filter(function(element) {
        return typeof element !== type
    });
      
}

const defArray = ['hello', 'world', 23, '23', null]
const filterType = prompt("Введіть назву типа даних, які потрібно вилучити", "number, string, object, undefined")

console.log(filterBy(defArray, filterType))